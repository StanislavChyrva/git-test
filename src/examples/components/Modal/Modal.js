import React from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';
import Button from "../Button/Button";

const Modal = (props) => {
    const { isOpen, setIsOpen, title, handleYesClick } = props;
    const closeModal = () => setIsOpen(false);

    if (!isOpen) return null;

    return (
        <div className={styles.root} >
            <div data-testid='Modal-bgd' className={styles.background} onClick={closeModal}/>
            <div className={styles.content}>
                <div className={styles.closeWrapper}>
                    <Button onClick={closeModal} className={styles.btn} variant="contained" color="error">X</Button>
                </div>
                <h2>{title}</h2>
                <div className={styles.buttonContainer}>
                    <Button onClick={handleYesClick} variant="contained" color="error">Yes</Button>
                    <Button onClick={closeModal} variant="contained">No</Button>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    isOpen: PropTypes.bool,
    setIsOpen: PropTypes.func,
    title: PropTypes.string,
    handleYesClick: PropTypes.func,
};
Modal.defaultProps = {
    isOpen: false,
    setIsOpen: () => {},
    title: '',
    handleYesClick: () => {},
};

export default Modal;