import Modal from './Modal';
import { render, screen, fireEvent } from '@testing-library/react';

import React, { useState } from 'react'

const Component = () => {
    const [isOpen, setIsOpen] = useState(false)
  return(
    <>
        <button onClick={ () => setIsOpen(prev => !prev) } >Change state</button>
        <Modal isOpen={isOpen} setIsOpen={setIsOpen} title="title" />
    </>
  )
  
}


describe('Modal renders', () => {
    test('should Modal no render without isOpen props', () => {
        const {asFragment} = render(<Modal isOpen={false}/>);
        expect(asFragment()).toMatchSnapshot();

    })
    test('should Modal render with props', () => {
        const {asFragment} = render(<Modal isOpen={true} title={'test title'}/>);
        expect(asFragment()).toMatchSnapshot();

    })

} )

describe('Modal close', () => {

    test('should Modal background close work', () => {
        render(<Component/>);

        fireEvent.click(screen.getByText('Change state'));

        const bgd = screen.getByTestId("Modal-bgd");

        expect(screen.getByTestId("Modal-bgd")).toBeInTheDocument();
        fireEvent.click(bgd)
        expect(screen.queryByTestId("Modal-bgd")).not.toBeInTheDocument();

    })

} )
