import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
    const { type, children, handleClick, className} = props;

    return (
        <>
          <button
              className={className}
              type={type}
              onClick={handleClick}
          >
              {children}
          </button>
        </>
    )
}

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit']),
    handleClick: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
};
Button.defaultProps = {
    type: 'button',
    handleClick: () => {},
    className: '',
};

export default Button;