import {render, screen, fireEvent} from "@testing-library/react"
import Button from "./Button"

const handleClick = jest.fn()

describe("Render Button", () => {
    test("should button render", () => {
        const {asFragment} = render(<Button type={"submit"} className="btn" >SUBMIT</Button>);
        expect(asFragment()).toMatchSnapshot()
    })
})

describe("Handle click at a button", () => {
    test("should handleClick work", () => {
        render(<Button type={"submit"} className="btn" handleClick={handleClick} >SUBMIT</Button>);
        const btn = screen.getByText("SUBMIT");
        fireEvent.click(btn)
        expect(handleClick).toHaveBeenCalled();
    })
})



// const { type, children, handleClick, className} = props;