import Title from "./index";
import { render, screen, fireEvent } from '@testing-library/react';

const handleContainerClick = jest.fn();

describe('Title snapshot testing', () => {
    test('should Title match snapshot without isEmpty', () => {
        const { asFragment } = render(<Title title="SOME TITLE" className="some_classes" />);
        expect(asFragment()).toMatchSnapshot();
    })

    test('should Title match snapshot with isEmpty', () => {
        const { asFragment } = render(<Title title="SOME TITLE" className="some_classes" isEpty={true} />);
        expect(asFragment()).toMatchSnapshot();
    })
});

describe('Title button works', () => {
    test('should paragrapgh render when click button', () => {
        render(<Title title="SOME TITLE" className="some_classes" />);
        
        const btn = screen.getByText('BUTTON');

        fireEvent.click(btn);
        expect(screen.getByText('paragraph')).toBeInTheDocument();

        fireEvent.click(btn);
        expect(screen.queryByText('paragraph')).not.toBeInTheDocument();
    });


    // test('should ELEMENT is render after timeout', async () => {
    //     render(<Title title="SOME TITLE" className="some_classes" />);

    //     expect(await screen.findByText('ELEMENT')).toBeInTheDocument();
    // });

    
    test('should container function works', async () => {
        render(<Title title="SOME TITLE" className="some_classes" handleContainerClick={handleContainerClick} />);

        fireEvent.click(screen.getByTestId('container'));
        expect(handleContainerClick).toHaveBeenCalled();
    });
})