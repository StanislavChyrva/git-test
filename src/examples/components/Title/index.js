import React, {useEffect, useState} from 'react';
import PropTypes from "prop-types";

const Title = ({ title, className, isEpty, handleContainerClick }) => {
    const [isParagraph, setIsParagraph] = useState(false);
    const [isElement, setIsElement] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setIsElement(true);
        }, 1000);
    }, [])

    if (isEpty) {
        return <h1 className={className}>{title}</h1>
    }

    return (
        <>
            <div data-testid="container" onClick={handleContainerClick}>  
                <h1 className={className}>{title}</h1>
                {isParagraph && <p>paragraph</p>}
                {isElement && <p>ELEMENT</p>}
                <button onClick={() => setIsParagraph(prev => !prev)}>BUTTON</button>
            </div>   
        </>
    )
}

Title.propTypes = {
    title: PropTypes.string,
    className: PropTypes.string,
    isEpty: PropTypes.bool,
    handleContainerClick: PropTypes.func,
};

Title.defaultProps = {
    title: 'Hello',
    className: '',
    isEpty: false,
    handleContainerClick: () => {},
};

export default Title;