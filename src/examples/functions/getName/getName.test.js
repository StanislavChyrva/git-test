import getName from './index'

const additionalCallback = jest.fn();

describe('get name returning correct values', () => {
    test('should getName return john if namechanger is null', () => {
        expect(getName()).toBe('John')
    })

    test('should name changer return john', () => {
        const fnToUpperCase = (value) => {
            return value.toUpperCase()
        }

        expect(getName(fnToUpperCase)).toBe('JOHN')
    })
});

describe('additional call back called', () => {
    test('should additional call back be called', () => {

        getName(null, additionalCallback);

        expect(additionalCallback).toHaveBeenCalled();
    })
});