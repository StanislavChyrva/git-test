import isInteger from "./index";

describe('isInteger works', () => {
    test('should return true if number is integer', () => {
        expect(isInteger(8)).toBeTruthy();
    });
    test('should return false because number is float', () => {
        expect(isInteger(0.5)).toBeFalsy();
    });
    test('should return false if arguments not number', () => {
        expect(isInteger('aaaaa')).toBeFalsy();
    });
    

})

