/**
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */
const sum = (a, b) => {
    if (!a || !b) {
        return null;
    }
    
    return a + b;
}

export default sum;