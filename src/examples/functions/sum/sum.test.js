import sum from './index';

describe('sum work', () => {
    test('should return the sum of 2 numbers', () => {
        expect(sum(5, 8)).toBe(13);
    }); 

    test('should return null without argument', () => {
        expect(sum(5)).toBeNull();
    }); 
});

